package model

import (
    "github.com/baldwmic/golang-github/api"
    "github.com/baldwmic/golang-github/repositories"
    "github.com/baldwmic/golang-github/users"
    "github.com/google/go-github/github"
    "io/ioutil"
    "encoding/json"
    "fmt"
)

/* Config file for model */
type RepositoryConfig struct {
    Owner string `json:"owner"`
    Name string `json:"name"`
}

type ModelConfig struct {
    Description string `json:"description"`
    Repositories []RepositoryConfig `json:"repositories"`
}

/* The Model */
type Model struct {
    Config *ModelConfig
    ApiClient *api.Api
}

/*
 * Create new model
 */
func New() (m *Model) {
    return new(Model)
}

/*
 * Configure and authenticate api client
 */
func (m *Model) ConfigureApi(fileName string) {
    a := api.New()
    a.Configure(fileName)
    a.Authenticate(a.Config.ApiKey)
    m.ApiClient = a
}

/*
 * Configure model using JSON file
 */
func (m *Model) ConfigureModel(fileName string) {

    // try to read file
    file, fileErr := ioutil.ReadFile(fileName)
    if fileErr != nil {
        fmt.Errorf("Error reading file: ", fileErr)
    }

    // create a config object to hold the json data
    var config ModelConfig

    // unmarshal the json
    unmarshalErr := json.Unmarshal(file, &config)
    if unmarshalErr != nil {
        fmt.Errorf("Error unmarshaling json: ", unmarshalErr)
    }

    // set the config object
    m.Config = &config
}

/* Data format for repositories */
// https://godoc.org/github.com/google/go-github/github#Repository
type RepositoryData struct {
    Owner string
    Name string
    Contributors []github.Contributor
}

/*
 * For each repository get the list of contributors.
 */
func (m *Model) GetRepositoryData(targetRepositories []RepositoryConfig) []RepositoryData {

    // slice of all repositories with data to serialize
    var allRepositories []RepositoryData

    // for each repository, get the list of contributors
    for _, repository := range targetRepositories {
        contributors := repositories.GetListContributors(m.ApiClient,
            repository.Owner, repository.Name)

        // formalize data for serialization
        r := RepositoryData{Owner: repository.Owner,
                            Name: repository.Name,
                            Contributors: contributors}
        allRepositories = append(allRepositories, r)
    }
    return allRepositories
}

/* Data format for users */
// https://godoc.org/github.com/google/go-github/github#User
type UserData struct {
    Login string
    Followers []github.User
    Following []github.User
}

/*
 * For each contributor in a repository, get the users who follow that contributor
 * and the users that contributor follows
 * https://godoc.org/github.com/google/go-github/github#Contributor
 * https://godoc.org/github.com/google/go-github/github#User
 */
func (m *Model) GetUserData(targetRepositories []RepositoryData) []UserData {

    // slice of all users with data to serialize
    var allUsers []UserData

    // track which users already visited using map
    visitedUsers := make(map[string]bool)

    // for each contributor in each repository, get who they follow
    for _, repository := range targetRepositories {
        for _, contributor := range repository.Contributors {
            loginName := *contributor.Login

            // if user not already visited
            if !visitedUsers[loginName] {
                visitedUsers[loginName] = true

                followers := users.GetFollowers(m.ApiClient, loginName)
                following := users.GetFollowing(m.ApiClient, loginName)

                // formalize data for serialization
                u := UserData{Login: loginName,
                              Followers: followers,
                              Following: following}
                allUsers = append(allUsers, u)
            }
        }
    }
    return allUsers
}
