package model

import (
    "testing"
)

/*
* Package: Data Model
* Author: Michael Baldwin
*/

const apiConfigFile = "../../../config/githubConfig.json"
const modelConfigFile = "../../../config/modelConfig.json"

/*
* Test creating new model
*/
func TestNew(t *testing.T) {

    // create new model
    m := New()

    // ApiClient should be nil
    if m.ApiClient != nil {
        t.Errorf("New(), Model.ApiClient == %q, expected %q", m.ApiClient, nil)
    }
}

/*
 * Test configuring api
 */
func TestConfigureApi(t *testing.T) {

    // create model, configure api
    m := New()
    m.ConfigureApi(apiConfigFile)

    // test that api configured
    t.Log(m.ApiClient.Config.Request.Limit)
}

/*
 * Test configuring model
 */
func TestConfigureModel(t *testing.T) {

    // create model, configure model
    m := New()
    m.ConfigureModel(modelConfigFile)

    // test that model configured
    t.Log(m.Config.Description)
    t.Log(m.Config.Repositories)
}

/*
 * Test getting repositories and their contributors
 */
func TestGetRepositoryData(t *testing.T) {

    // create model, configure api, configure model
    m := New()
    m.ConfigureApi(apiConfigFile)
    m.ConfigureModel(modelConfigFile)

    // get repositories and their contributors
    repositories := m.GetRepositoryData(m.Config.Repositories)

    // test that contributors returned
    t.Log(len(repositories))
}

/*
 * Test getting contributor data, like followers
 */
func TestGetUserData(t *testing.T) {

    // create model, configure api, configure model
    m := New()
    m.ConfigureApi(apiConfigFile)
    m.ConfigureModel(modelConfigFile)

    // get repositories and their contributors
    repositories := m.GetRepositoryData(m.Config.Repositories)

    // get users and their followers
    users := m.GetUserData(repositories)

    // test that users returned
    t.Log(len(users))
}
