package serialize

import (
    "io/ioutil"
    "encoding/json"
    "fmt"
)

// https://michaelheap.com/golang-encodedecode-arbitrary-json/
func ReadJson(fileName string, object interface{}) {

    // try to read the file
    file, fileErr := ioutil.ReadFile(fileName)
    if fileErr != nil {
        fmt.Errorf("Error reading file: ", fileErr)
    }

    // unmarshal the json into an object
    unmarshalErr := json.Unmarshal(file, &object)
    if unmarshalErr != nil {
        fmt.Errorf("Error unmarshaling json into object: ", unmarshalErr)
    }
}

func WriteJson(fileName string, object interface{}) {

    // try to marshal the object into json with pretty print style, 4 spaces for indent
    jsonObject, marshalErr := json.MarshalIndent(object, "", "    ")
    if marshalErr != nil {
        fmt.Errorf("Error marshaling object into json: ", marshalErr)
    }

    // try to write to file
    fileErr := ioutil.WriteFile(fileName, jsonObject, 0644)
    if fileErr != nil {
        fmt.Errorf("Error writing file: ", fileErr)
    }
}
