package main

import (
    "github.com/baldwmic/hnr499/src/golang/model"
    "github.com/baldwmic/hnr499/src/golang/model/serialize"
    "github.com/baldwmic/golang-github/miscellaneous/ratelimit"
    "flag"
    "fmt"
)

func main() {
    m := configureModel()
    queryData(m)
}

/*
 * Parse command line arguments with defaults and help descriptions.
 */
func parseFlags(m *model.Model) (*int) {
    requests := flag.Int("requests", m.ApiClient.Config.Request.Limit, "The maximum number of Github API requests to make.")
    flag.Parse()
    return requests
}

/*
 * Log Github API requests remaining
 */
func logRequestsRemaining(m *model.Model) {
    remaining, err := ratelimit.GetCoreRemaining(m.ApiClient)
    if err != nil {
        fmt.Errorf("Error logging requests remaining: %v\n", err)
    } else {
        fmt.Printf("Github API requests remaining: %d\n", remaining)
    }
}

/*
 * Configure model from JSON files
 */
func configureModel() *model.Model {

    // create model, configure api, configure model
    m := model.New()
    m.ConfigureApi("config/githubConfig.json")
    m.ConfigureModel("config/modelConfig.json")

    // parse command line arguments to override desired settings
    requests := parseFlags(m)
    m.ApiClient.Config.Request.Limit = *requests

    return m
}

/*
 * Query data from Github API
 */
func queryData(m *model.Model) {
    logRequestsRemaining(m)

    // get list of contributors for each repository
    repositories := m.GetRepositoryData(m.Config.Repositories)
    serialize.WriteJson("data/github/repositories.json", repositories)

    // get those users each contributor follows
    users := m.GetUserData(repositories)
    serialize.WriteJson("data/github/users.json", users)

    logRequestsRemaining(m)
}
