import json

# read json file into python dictionary
def read_json(json_file):
    with open(json_file, 'rb') as data_file:
        data = json.loads(data_file.read())
        return data

# write python data to csv file with header
def write_csv(csv_file, header, data_list):
    with open(csv_file, 'wb') as data_file:
        data_file.write(header + '\n')
        for item in data_list:
            data_file.write(str(item) + '\n')

# read python data from csv file into list of lists
def read_csv(csv_file, skip_header):
    data_list = []
    with open(csv_file, 'rb') as data_file:
        for line in data_file:
            lineList = line.split(',')
            lineList[-1] = lineList[-1].strip('\n')
            data_list.append(lineList)
    if skip_header:
        data_list = data_list[1:]
    return data_list
