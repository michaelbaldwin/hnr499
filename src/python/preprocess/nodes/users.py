import serialize

# http://neo4j.com/docs/stable/import-tool-header-format.html
# http://neo4j.com/docs/stable/import-tool-examples.html

csv_header = 'userId:ID(User),login,:LABEL'

class User:

    def __init__(self, id, login):
        self.id = id
        self.login = login
        self.label = 'User'

    def __str__(self):
        values = []
        values.append(self.id)
        values.append(self.login)
        values.append(self.label)
        return ','.join(map(str, values))

class Users:

    def __init__(self):
        self.users = []
        self.id_sequence = 1
        self.login_visited = {}

    def add(self, user):
        # only add those users not already visited
        if user.login not in self.login_visited:
            self.login_visited[user.login] = True
            self.users.append(user)
            self.id_sequence += 1

def parse_users(json_file):
    users = serialize.read_json(json_file)

    # create users with unique ids
    allUsers = Users()
    for user in users:
        allUsers.add(User(allUsers.id_sequence, user['Login']))

        followers = user['Followers']
        if followers is not None:
            for follower in followers:
                allUsers.add(User(allUsers.id_sequence, follower['login']))

        following = user['Following']
        if following is not None:
            for follow in following:
                allUsers.add(User(allUsers.id_sequence, follow['login']))

    return allUsers

def create_csv(json_file, csv_file):
    parsed_users = parse_users(json_file)
    serialize.write_csv(csv_file, csv_header, parsed_users.users)
