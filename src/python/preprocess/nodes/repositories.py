import serialize

# http://neo4j.com/docs/stable/import-tool-header-format.html
# http://neo4j.com/docs/stable/import-tool-examples.html

# header needs to represent list of attributes sorted alphabetically ascending
csv_header = 'repositoryId:ID(Repository),owner,name,:LABEL'

class Repository:

    def __init__(self, id, owner, name):
        self.id = id
        self.owner = owner
        self.name = name
        self.label = 'Repository'

    def __str__(self):
        values = []
        values.append(self.id)
        values.append(self.owner)
        values.append(self.name)
        values.append(self.label)
        return ','.join(map(str, values))

class Repositories:

    def __init__(self):
        self.repositories = []
        self.id_sequence = 1

    def add(self, repository):
        self.repositories.append(repository)
        self.id_sequence += 1

def parse_repositories(json_file):
    repositories = serialize.read_json(json_file)

    # create repositories with unique ids
    allRepositories = Repositories()
    for repository in repositories:
        allRepositories.add(Repository(allRepositories.id_sequence, repository['Owner'], repository['Name']))
    return allRepositories

def create_csv(json_file, csv_file):
    parsed_repositories = parse_repositories(json_file)
    serialize.write_csv(csv_file, csv_header, parsed_repositories.repositories)
