from nodes import repositories
from nodes import users
from relationships import follows
from relationships import contributes

def main():

    # preprocess data for repositories
    repositories_json = 'data/github/repositories.json'
    repositories_csv = 'data/neo4j/nodes/repositories.csv'
    repositories.create_csv(repositories_json, repositories_csv)

    # preprocess data for users
    users_json = 'data/github/users.json'
    users_csv = 'data/neo4j/nodes/users.csv'
    users.create_csv(users_json, users_csv)

    # preprocess data for follows
    follows_csv = 'data/neo4j/relationships/follows.csv'
    follows.create_csv(users_csv, users_json, follows_csv)

    # preprocess data for contributes
    contributes_csv = 'data/neo4j/relationships/contributes.csv'
    contributes.create_csv(users_csv, repositories_csv, repositories_json, contributes_csv)

if __name__ == "__main__":
    main()
