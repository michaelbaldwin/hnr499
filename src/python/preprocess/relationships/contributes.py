import serialize

csv_header = ':START_ID(User),:END_ID(Repository),contributions,:TYPE'

class Contribute:

    def __init__(self, start_id, end_id, contributions):
        self.start_id = start_id
        self.end_id = end_id
        self.contributions = contributions
        self.type = 'CONTRIBUTES'

    def __str__(self):
        values = []
        values.append(self.start_id)
        values.append(self.end_id)
        values.append(self.contributions)
        values.append(self.type)
        return ','.join(map(str, values))

def parse_users(users_csv_file):
    # read csv file of users, skipping the header
    users = serialize.read_csv(users_csv_file, True)
    # map login to unique id
    login_to_id = {}
    for user in users:
        login_to_id[user[1]] = user[0]
    return login_to_id

def parse_repositories(repositories_csv_file):
    repositories = serialize.read_csv(repositories_csv_file, True)
    # map owner and name concatenated to unique id
    owner_name_to_id = {}
    for repository in repositories:
        owner_name_to_id[repository[1] + repository[2]] = repository[0]
    return owner_name_to_id


def parse_contributes(repositories_json_file, login_to_id, owner_name_to_id):
    repositories = serialize.read_json(repositories_json_file)

    # get all contributes relationships
    allContributes = []
    for repository in repositories:
        repository_id = owner_name_to_id[repository['Owner'] + repository['Name']]
        contributors = repository['Contributors']
        for contributor in contributors:
            user_id = login_to_id[contributor['login']]
            contributions = contributor['contributions']
            allContributes.append(Contribute(user_id, repository_id, contributions))

    return allContributes

def create_csv(users_csv_file, repositories_csv_file, repositories_json_file, csv_file):
    # generate dictionary of login to id so can use ids for relationship
    login_to_id = parse_users(users_csv_file)
    # generate dictionary of owner and name to id so can use for relationship
    owner_name_to_id = parse_repositories(repositories_csv_file)
    contributes = parse_contributes(repositories_json_file, login_to_id, owner_name_to_id)
    serialize.write_csv(csv_file, csv_header, contributes)
