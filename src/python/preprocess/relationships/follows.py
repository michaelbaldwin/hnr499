import serialize

csv_header = ':START_ID(User),:END_ID(User),:TYPE'

class Follow:

    def __init__(self, start_id, end_id):
        self.start_id = start_id
        self.end_id = end_id
        self.type = 'FOLLOWS'

    def __str__(self):
        values = []
        values.append(self.start_id)
        values.append(self.end_id)
        values.append(self.type)
        return ','.join(map(str, values))

def parse_users(users_csv_file):
    # read csv file of users, skipping the header
    users = serialize.read_csv(users_csv_file, True)
    # map login to unique id
    login_to_id = {}
    for user in users:
        login_to_id[user[1]] = user[0]
    return login_to_id

def parse_follows(users_json_file, login_to_id):
    users = serialize.read_json(users_json_file)

    # get all follows relationships
    allFollows = []
    for user in users:

        # get follows relationships from who follows the user
        followers = user['Followers']
        if followers is not None:
            for follower in followers:
                start_id = login_to_id[follower['login']]
                end_id = login_to_id[user['Login']]
                allFollows.append(Follow(start_id, end_id))

        # get follows relationships from who the user is following
        following = user['Following']
        if following is not None:
            for follow in following:
                start_id = login_to_id[user['Login']]
                end_id = login_to_id[follow['login']]
                allFollows.append(Follow(start_id, end_id))

    return allFollows

def create_csv(users_csv_file, users_json_file, csv_file):
    # generate dictionary of login to id so can use ids for relationships
    login_to_id = parse_users(users_csv_file)
    follows = parse_follows(users_json_file, login_to_id)
    serialize.write_csv(csv_file, csv_header, follows)
