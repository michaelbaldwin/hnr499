import serialize
import visualize

# py2neo cypher runs out of memory when outputting json, so need to clean
# csv output where values not truly comma separated due to nested data, and
# last line is empty
def clean_cypher_data(data_list):
    # get header and strip double quotes "" from header items
    header = data_list[:1][0]
    header = map((lambda x: str(x).strip('\"')), header)
    # get data, exclude header and last line since empty
    data_rows = data_list[1:-1]
    cleaned_data = list()
    for row in data_rows:
        # first four columns are not comma separated
        merged = ''.join(row[:4])
        # remove those columns
        row = row[4:]
        # insert merged data
        row.insert(0, merged)
        cleaned_data.append(row)
    return header, cleaned_data

# convert list of lists to list of dictionaries
def lists_to_dicts(header, data):
    dicts = list()
    for item in data:
        item_dict = dict()
        for idx, attr in enumerate(header):
            item_dict[attr] = item[idx]
        dicts.append(item_dict)
    return dicts

# get column of data, where data is list of dictionaries
def read_column(data_dicts, column_name):
    column_data = []
    for data_point in data_dicts:
        column_data.append(data_point[column_name])
    return column_data

def degree_distribution(data_dicts, viz_file):
    in_degree = read_column(data_dicts, 'in_degree')
    out_degree = read_column(data_dicts, 'out_degree')
    title = 'User Degree Distribution'
    xaxis = 'Degree'
    yaxis = 'Frequency'
    plotly_url = visualize.plotly_histogram_overlaid(in_degree, 'in_degree', out_degree, 'out_degree', xaxis, yaxis, title, viz_file)
    print plotly_url

def main():
    data_degree_csv = 'data/neo4j/queries/degree.csv'
    viz_degree_png = 'viz/degree_histogram.png'
    # read in cypher csv output
    csv_data = serialize.read_csv(data_degree_csv, ',', '\n')
    header, data = clean_cypher_data(csv_data)
    data_dicts = lists_to_dicts(header, data)
    # generate histogram
    degree_distribution(data_dicts, viz_degree_png)

if __name__=='__main__':
    main()
