import serialize
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go

# assuming running from root directory
config_file = 'config/plotlyConfig.json'

# https://plot.ly/python/reference/
def configure(config_json_file):
    configuration = serialize.read_json(config_json_file)
    py.sign_in(configuration['username'], configuration['apikey'])

# https://plot.ly/python/histograms/
# https://plot.ly/python/histograms-and-box-plots-tutorial/
# https://plot.ly/python/static-image-export/
def plotly_histogram_overlaid(data_one, name_one, data_two, name_two, xaxis_title, yaxis_title, title_text, file_name):
    configure(config_file)
    x0 = np.array(data_one)
    x1 = np.array(data_two)
    trace1 = go.Histogram(
        x=x0,
        opacity=0.75,
        name=name_one
    )
    trace2 = go.Histogram(
        x=x1,
        opacity=0.75,
        name=name_two
    )
    data = [trace1, trace2]
    layout = go.Layout(
        barmode='overlay',
        title=title_text,
        xaxis=dict(
            title=xaxis_title
        ),
        yaxis=dict(
            title=yaxis_title
        )
    )
    fig = go.Figure(data=data, layout=layout)
    py.image.save_as(fig, filename=file_name)
    plot_url = py.plot(fig, filename=file_name, auto_open=False)
    return plot_url

