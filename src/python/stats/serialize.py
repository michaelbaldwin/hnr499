import json

# read json file into python dictionary
def read_json(json_file):
    with open(json_file, 'rb') as data_file:
        data = json.loads(data_file.read())
        return data

# read csv file into python list of lists
def read_csv(csv_file, delimiter, strip):
    with open(csv_file, 'rb') as data_file:
        data_list = list()
        for line in data_file:
            line = line.strip(strip)
            tokens = line.split(delimiter)
            data_list.append(tokens)
        return data_list
