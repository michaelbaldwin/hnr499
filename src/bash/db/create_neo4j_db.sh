#!/bin/bash

# @purpose: create network of data from github

# command line arguments
# $1 = requests

# Query Github API

# Compile GoLang program, execute to query the Github API and output to JSON
echo "Querying Github API..."
mkdir -p data/github
go install github.com/baldwmic/hnr499/src/golang/querygithub
$GOPATH/bin/querygithub
echo ""

# Preprocess Data

# Python script converts Github JSON data to CSV data for Neo4j database
echo "Preprocessing data..."
mkdir -p data/neo4j/nodes
mkdir -p data/neo4j/relationships
python src/python/preprocess/preprocess_neo4j.py
echo ""

# Import Data into Neo4j

# Sudo to neo4j user and feed commands to heredoc
# remove the old database
# import the new database
echo "Importing data into Neo4j database..."
sudo -u neo4j bash << EOF
rm -rf /var/lib/neo4j/data/graph.db/*
neo4j-import --into /var/lib/neo4j/data/graph.db/ --nodes data/neo4j/nodes/repositories.csv --nodes data/neo4j/nodes/users.csv --relationships data/neo4j/relationships/contributes.csv --relationships data/neo4j/relationships/follows.csv
EOF
echo ""

# Backup Neo4j

# Backup Neo4j database to a tar file
echo "Backing up Neo4j database..."
mkdir -p data/neo4j/db 
cd /var/lib/neo4j/data
tar -cjf ~/goProjects/src/github.com/baldwmic/hnr499/data/neo4j/db/graph.tar.bz2 graph.db
cd ~/goProjects/src/github.com/baldwmic/hnr499
echo ""

# Restart Neo4j server
echo "Restarting Neo4j server..."
sudo service neo4j-service restart
echo ""
