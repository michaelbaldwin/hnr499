#!/bin/bash

# @purpose: automate project pipeline for hnr499, including
# generating graph database, statistics and visualizations

# Generate Neo4j Database
echo "Generating Neo4j database..."
sh src/bash/db/create_neo4j_db.sh
echo ""

# Generate Statistics and Visualizations
echo "Generating statistics and visualizations..."
sh src/bash/stats/statistics.sh
echo ""
