#!/bin/bash

# @purpose: generate statistics and visualizations

# Degree Distribution
echo "Calculating Degree Distribution..."
sh src/bash/stats/degree_distribution.sh
echo ""

echo "Visualizing Degree Distribution"
python src/python/stats/degree_distribution.py
echo ""
