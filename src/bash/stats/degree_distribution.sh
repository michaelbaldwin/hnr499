#!/bin/bash

# @purpose: query neo4j database for degree distribution

# Degree Distribution
echo "Querying Neo4j for Degree Distribution..."
mkdir -p data/neo4j/queries
cypher --csv "MATCH (n:User) WITH n, size((n)-[:FOLLOWS]->()) as out_degree, size((n)<-[:FOLLOWS]-()) as in_degree RETURN n, out_degree, in_degree, out_degree+in_degree as degree" > data/neo4j/queries/degree.csv
echo ""
