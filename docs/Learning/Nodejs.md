# Learning NodeJS

## NodeJS

## Installing NodeJs on Ubuntu
- https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server
```
sudo apt-get update
sudo apt-get install build-essential libssl-dev
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.30.2/install.sh | bash
nvm install 5.0
```

## Install Node Modules
- https://nodejs.org/api/modules.html
- http://stackoverflow.com/questions/5817874/how-do-i-install-a-module-globally-using-npm

## Install Neo
- https://www.npmjs.com/package/node-neo4j
```
npm install node-neo4j
```

## Issues with Node loading
- Logged out and node no longer recognized as a program
- need to issue command: `nvm use 5.0`
- added to ~/.bash_aliases which is run each login
- moved loading nvm to ~/.bash_aliases
