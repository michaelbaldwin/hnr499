# Learning Python

## Python

## [Install Pip](https://pip.pypa.io/en/stable/installing/)
```
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
```

### Modules
- http://stackoverflow.com/questions/15746675/how-to-write-a-python-module

### Classes
- https://jeffknupp.com/blog/2014/06/18/improve-your-python-python-classes-and-object-oriented-programming/
- http://www.saltycrane.com/blog/2008/09/how-iterate-over-instance-objects-data-attributes-python/
- https://docs.python.org/2/library/functions.html#dir
- http://stackoverflow.com/questions/4075190/what-is-getattr-exactly-and-how-do-i-use-it

### List Comprehensions
- http://www.secnetix.de/olli/Python/list_comprehensions.hawk

### Functional Programming
- http://www.python-course.eu/lambda.php
- https://docs.python.org/3/library/stdtypes.html#str.join
- http://stackoverflow.com/questions/44778/how-would-you-make-a-comma-separated-string-from-a-list
- https://docs.python.org/2/library/functions.html#map
- https://docs.python.org/2/library/functions.html#filter
- http://www.diveintopython.net/power_of_introspection/index.html
- http://www.u.arizona.edu/~erdmann/mse350/topics/list_comprehensions.html

### How To
#### JSON
- http://stackoverflow.com/questions/2835559/parsing-values-from-a-json-file-in-python
#### CSV
- https://docs.python.org/2/library/csv.html#
