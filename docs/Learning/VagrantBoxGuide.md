# Vagrant Box for GoLang development

## 1. install vagrant and virtualbox on mac


## 2. create vagrant box using ubuntu
- vagrant init ubuntu/trusty64
- vagrant up
- vagrant ssh


## 3. install git, vim
- sudo apt-get update
- sudo apt-get upgrade
- sudo apt-get install vim
- sudo apt-get install git


## 4. install [golang](https://golang.org/doc/install) on virtual machine
- curl -O https://storage.googleapis.com/golang/go1.5.3.linux-amd64.tar.gz
- sudo tar -C /usr/local/ -xzf go1.5.3.linux-amd64.tar.gz
- export PATH=$PATH:/usr/local/go/bin
- run `go`


## 5. test [go](https://golang.org/doc/code.html) installation
- mkdir goProjects
- export GOPATH=$HOME/goProjects
- mkdir -p goProjects/src/github.com/baldwmic/hello
- vim goProjects/src/github.com/baldwmic/hello/hello.go
- go install github.com/baldwmic/hello
- $GOPATH/bin/hello


## 6. package vagrant box
- sudo-apt get clean
- sudo dd if=/dev/zero of=/EMPTY bs=1M
- sudo rm -f /EMPTY
- cat /dev/null > ~/.bash_history && history -c && exit
- vagrant package --output hnr499.box


## 7. add newly packaged vagrant box to vagrant
- vagrant box add mynewbox hnr499.box


## 8. remove old vagrant box and file
- vagrant destroy
- rm Vagrantfile


## 9. create new vagrant setup
- vagrant init hnr499
- vagrant up
- vagrant ssh

## 10. fix vb guest additions
- https://github.com/dotless-de/vagrant-vbguest/


## 11. upload vagrant box to atlas vagrant cloud
- https://vagrantcloud.com/help/vagrant/boxes/create-version

## 12. extend memory available for box
- https://www.vagrantup.com/docs/virtualbox/configuration.html

## 13. install neo4j on vagrant box
- http://www.darwinbiler.com/installing-neo4j-in-vagrant/



# Version a box

- create a copy of the virtualbox virtual machine
- test that installation of things works, golang, node, neo4j, etc.
- then go into that box and clean things up, zero hard drive, clear bash history, etc.
- then package that cleaned up (copy) of virtual machine and export to box and upload to atlas

- Go to atlas web interface https://atlas.hashicorp.com/baldwmic/boxes/hnr499
- create a new version
- create the provider and upload the newly
versioned box

- go to the github directory where use vagrant box
- check for update
- download the newest version
- changes should be saved to virtualbox, where the virtual machine is (running package converts vm to a box for vagrant, but is a one time deal, doesnt do this automatically)
