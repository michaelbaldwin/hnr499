# Py2Neo

## Learning Py2Neo

### [Authentication](http://py2neo.org/2.0/essentials.html#authentication)
- Set NEO4J_AUTH and NEO4J_URI environment variables (added to ~/.bash_aliases)
```
export NEO4J_URI="rest_url"
export NEO4J_AUTH="username:password"
```

### [Cypher](http://py2neo.org/2.0/cypher.html#command-line)
- Execute cypher from command line, output to files
- Example to test that authentication working:
```
cypher "MATCH (n) RETURN n"
```
