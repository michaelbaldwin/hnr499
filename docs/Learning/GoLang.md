# Learning GoLang

## GoLang
- [Getting Started with Go](https://golang.org/doc/install)
- [How to Write Go Code](https://golang.org/doc/code.html#Workspaces)
- [Tour of Go](https://tour.golang.org/welcome/1)
- [GoLang Cheat Sheet](https://github.com/a8m/go-lang-cheat-sheet)
- [Github API](https://godoc.org/github.com/google/go-github/github)

### Commands
#### compile go program with main package
```
go install github.com/baldwmic/hnr499
```
#### execute go program compiled
```
$GOPATH/bin/hnr499
```
#### build a go library
```
go build github.com/baldwmic/datastructures
```
#### test go library
```
go test github.com/baldwmic/
```
#### test go library with verbose mode
```
go test -v github.com/baldwmic/golang-github/api
```
#### get/download go library from github
```
go get github.com/baldwmic/golang-datastructures
```
#### update golang dependencies
```
go get -u all
```

### Packages
- [Install Go Packages from Github](http://stackoverflow.com/questions/10772799/how-to-install-golang-3rd-party-projects-from-download-sources)
- [TypeChecking Loop](http://engblog.yext.com/post/demystifying-typechecking-loop-errors)

### Structs
- [Import Struct From Other Package](http://stackoverflow.com/questions/29898400/import-struct-from-another-package-and-file-golang)

### Maps
- [Using Maps](https://tour.golang.org/moretypes/19)
- [Go Maps](https://blog.golang.org/go-maps-in-action)

### Interface
- [Convert Interface to String](http://stackoverflow.com/questions/14289256/cannot-convert-data-type-interface-to-type-string-need-type-assertion)

### Strings
- [Concatenate Strings](http://herman.asia/efficient-string-concatenation-in-go)
- [I/O to Write Strings](https://gobyexample.com/writing-files)

### Error Handling
- [Handling Errors](http://blog.golang.org/error-handling-and-go)

### Constants
- [Go Constants](https://blog.golang.org/constants)

### Config File
- [Viper for Config File](https://github.com/spf13/viper)
- [Read File Line by Line](http://stackoverflow.com/questions/8757389/reading-file-line-by-line-in-go)
- [JSON Config File](http://stackoverflow.com/questions/16465705/how-to-handle-configuration-in-go)
- [Reading JSON](http://stackoverflow.com/questions/16681003/how-do-i-parse-a-json-file-into-a-struct-with-go)
- [Unmarshal JSON](http://stackoverflow.com/questions/25966567/go-unmarshal-nested-json-structure)

### Testing
- [Testing Examples](https://blog.golang.org/examples)

### Slices
- [Append Slice to Slice using Ellipsis](http://www.golangbootcamp.com/book/collection_types)

### IO
- [Writing Files](https://gobyexample.com/writing-files)
- [JSON and Go](http://blog.golang.org/json-and-go)
- [Json Pretty Print](http://stackoverflow.com/questions/19038598/how-can-i-pretty-print-json-using-go)
