# Neo4j queries using the Cypher querying language

## Degree Distribution
- https://groups.google.com/forum/#!msg/neo4j/CQJXB-spc5E/PT_ZuortO-QJ
MATCH (n)
WITH n, size((n)-[:FOLLOWS]->()) as out_degree, size((n)<-[:FOLLOWS]-()) as in_degree
RETURN n, out_degree, in_degree, out_degree+in_degree as degree;

- command line, run cypher query and output to csv
```
cypher --csv "MATCH (n) WITH n, size((n)-[:FOLLOWS]->()) as out_degree, size
((n)<-[:FOLLOWS]-()) as in_degree RETURN n, out_degree, in_degree, out_degree+in_degree as degree" > data/neo4j/queries/degree.csv
```

## Triangles, count?
- https://www.airpair.com/neo4j/posts/getting-started-with-neo4j-and-cypher
```
MATCH (a)-->(b)-->(c), (c)-->(a)
RETURN a, b, c
```
