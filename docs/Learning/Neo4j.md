# Learning Neo4j

## Neo4J
- [Neo4j Introduction](http://neo4j.com/docs/stable/introduction.html)
- [Neo4j PDF](http://info.neo4j.com/rs/neotechnology/images/WhatisNeo4j.pdf)

### Ubuntu Service
#### Start Neo4J service on Ubuntu
```
sudo service neo4j-service start
```
#### Stop Neo4J service on Ubuntu
```
sudo service neo4j-service stop
```
#### Server Directory, Properties File
```
cd /var/lib/neo4j
vim conf/neo4j-server.properties
```
#### Authentication File Location
- Edit the properties file to enable authentication
- http://stackoverflow.com/questions/27645951/how-to-configure-user-and-password-for-neo4j-cluster-without-rest-api
```
sudo vim /var/lib/neo4j/data/dbms/auth
```
#### Neo4j [database location](http://stackoverflow.com/questions/10888280/neo4j-how-to-switch-database)
```
cd /var/lib/neo4j/data/graph.db
```
#### Neo4j server
```
http://localhost:7474/
```
#### Neo4j version
```
http://stackoverflow.com/questions/10881485/neo4j-how-to-get-current-version-via-rest
```

### Shell
#### Start [Neo4J shell](http://neo4j.com/docs/stable/shell.html)
```
neo4j-shell
```

### Import Tool
#### into
```
--into <store-dir>
```
#### nodes
```
--nodes[:Label1:Label2] "<file1>,<file2>,…"
```
#### edges
```
--relationships[:RELATIONSHIP_TYPE] "<file1>,<file2>,…"
```

### Cypher
#### Return all nodes
```
MATCH (n)
RETURN n;
```
#### Return all edges ???
```
MATCH ()-[r:RELTYPE]->()
RETURN r;
```
#### Delete all nodes and edges
```
MATCH (node)
OPTIONAL MATCH (node)=[edge]-()
DELETE node, edge;
```

#### Create node
```
CREATE (me:Person {name: "Michael"}) return me
```
#### Create relationship (edge)
```
MATCH (student:Person), (professor:Person)
WHERE student.name = "Michael"
AND professor.name = "Roger"
CREATE (professor)-[:TEACHES {class:"HNR499"}]->(student);
```
#### Create only one relationship (MERGE)
```
MATCH (clint:Person),(mystic:Movie)      
WHERE clint.name="Clint Eastwood" AND mystic.title="Mystic River"
MERGE (clint)-[:DIRECTED]->(mystic)
RETURN clint, mystic;
```
#### Delete node and any relationships might belong to node
```
MATCH (me:Person {name="My Name"})
OPTIONAL MATCH (me)-[r]-()
DELETE me,r;
```
#### Retrieve nodes who had relationship type to other node
```
MATCH (node1)-[:REL_TYPE]->(node2)
```
```
MATCH (actor)-[:ACTED_IN]->(movie)
RETURN actor.name, movie.title
```
#### Retrieve relationship info
```
MATCH (node1)-[rel:TYPE]->(node2)
RETURN rel.property
```
#### Filtering matching using Labels (Groups of nodes)
```
MATCH (node1:Label1)-[:REL_TYPE]->(node2:Label2)
RETURN node1, node2
```
#### Path
```
MATCH p1=(a)-[:ACTED_IN]->(m), p2=(d)-[:DIRECTED]->(m)
RETURN p1, p2;
```
#### Filtering results
```
MATCH (keanu:Person)-[r:ACTED_IN]->(movie)
WHERE keanu.name="Keanu Reeves"
AND "Neo" IN r.roles
RETURN movie.title;
```
#### Distinct, Order, Limit
```
MATCH (a)-[:ACTED_IN]->()
RETURN DISTINCT a
ORDER BY a.born
LIMIT 5
```

### Ubuntu
- [Install Neo4j on Ubuntu](http://debian.neo4j.org/)
- [Neo4j on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-neo4j-on-an-ubuntu-vps)
- [Neo4j Data Location on Ubuntu](http://stackoverflow.com/questions/20083966/what-is-the-fully-qualified-path-of-the-neo4j-data-directory-on-ubuntu-12-04)

### Cypher
- [Neo4j Manual](http://neo4j.com/docs/2.0.0/cypher-query-lang.html)
- [Neo4j Useful Queries](http://www.remwebdevelopment.com/blog/sql/some-basic-and-useful-cypher-queries-for-neo4j-201.html)
- [Transaction Failure Exception](http://stackoverflow.com/questions/17371615/neo4j-unable-to-delete-multiple-nodes-i-get-a-transaction-failure-exception)

### Load Data
- [Neo4j load data](http://neo4j.com/docs/stable/cypherdoc-loading-data.html)
- [Load CSV data](http://neo4j.com/docs/stable/query-load-csv.html)
- [Nodes and Relationships](http://neo4j.com/docs/stable/cypherdoc-patterns-in-practice.html)
- [CSV Required Format](http://neo4j.com/docs/stable/import-tool-header-format.html)
- [Load CSV using Cypher](http://neo4j.com/docs/stable/cypherdoc-importing-csv-files-with-cypher.html)

### Hosting
- http://www.graphenedb.com/
- http://elastx.com/our-blog/neo4j-in-the-cloud-easy-with-elastx#.Vu8TMpMrKRs
- http://www.neo4j.org/develop/ec2_manual

### Graphenedb
- http://www.graphenedb.com/docs/getting-started.html
- data is exported when run bash script
```
cd /var/lib/neo4j/data
tar -cjf ~/goProjects/src/github.com/baldwmic/hnr499/data/neo4j/db/graph.tar.bz2 graph.db
```
- upload via web interface
```
1. login to graphenedb
2. admin tab
3. restore database, upload from local file
```

### Degree Distribution
- https://groups.google.com/forum/#!msg/neo4j/CQJXB-spc5E/PT_ZuortO-QJ
