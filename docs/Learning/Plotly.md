# Learning Plotly

## Plotly

### Install
- https://plot.ly/python/getting-started
- https://plot.ly/python/overview/
```
pip install plotly
```
### API
```
py.sign_in('username', 'apikey')
```

### Histogram
- https://plot.ly/python/histograms/
- https://bespokeblog.wordpress.com/2011/07/11/basic-data-plotting-with-matplotlib-part-3-histograms/
- http://help.plot.ly/make-a-histogram/
