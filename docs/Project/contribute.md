# Contribute
Guide to get up and running on Debian.
Note: Really should provision Vagrant box using [Chef](http://www.dragdropsite.com/tutorials/creating-development-environments-vagrant-chef/).

## Install Dependencies

### GoLang
#### Install GoLang
```
wget https://storage.googleapis.com/golang/go1.6.linux-armv6l.tar.gz
tar -C /usr/local -xzf go1.6.linux-armv6l.tar.gz
export PATH=$PATH:/usr/local/go/bin
```
#### Export GoLang Path
```
mkdir ~/goProjects
export GOPATH=$HOME/goProjects
```


### Neo4j
#### Install Neo4j
```
sudo su
wget -O - http://debian.neo4j.org/neotechnology.gpg.key | apt-key add -
echo 'deb http://debian.neo4j.org/repo stable/' > /etc/apt/sources.list.d/neo4j.list
apt-get update
apt-get install neo4j
```


## Get Source
### Get Repository with Go
```
go get github.com/baldwmic/hnr499
```
### Install GoLang Dependencies
```
go get github.com/baldwmic/golang-github
go get github.com/google/go-github/github
go get golang.org/x/oauth2
```
### Create Github Config File (format in config.md)
```
vim config/githubConfig.json
```

### Install Python Dependencies
```
sudo pip install py2neo
sudo pip install plotly
```
### Create Plotly Config File (format in config.md)
```
vim config/plotlyConfig.json
```
### Export py2neo authentication (password is set when first login)
```
export NEO4J_URI="http://localhost:7474/db/data"
export NEO4J_AUTH="neo4j:password"
```
