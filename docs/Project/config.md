# Configuration Files
- Location: 'config' directory
- Note: Any config files with API keys are .gitignored

## Github Repositories:
- [Repositories with Most Stars](https://github.com/search?q=stars:%3E1&s=stars&type=Repositories)

### githubConfig.json
```
{
    "apikey": "insert Github API Personal Access Token",
    "request": {
        "limit": {insert max number requests}
    }
}
```

### modelConfig.json
```
{
    "description": "Github network of contributors to repositories.",
    "repositories": [
        {
            "owner": "adamtwig",
            "name": "D4D"
        },
        {
            "owner": "engelsjo",
            "name": "DocumentClassifier"
        }
    ]
}
```

### plotlyConfig.json
```
{
    "username": "insert username",
    "apikey": "insert api key"
}
```

