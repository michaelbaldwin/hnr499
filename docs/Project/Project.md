# Learning

## Research
- [Graphing Github](http://neo4j.com/blog/keylines-graphing-github/)
- [Current Github Graphs](https://help.github.com/articles/about-repository-graphs/)
- [Build Network Visualizations](http://keylines.com/network-visualization/building-great-network-visualizations)
- [Christophe Willemsen](https://github.com/ikwattro?tab=repositories)

## Paper
### Title
- How the Economics of Open Source Software Impacts Diversity of Thought in Software Development

### Description
- Gained insight into the societal impact of current, modern development of open source software projects through social network analysis of organizational and user contributions to popular Github repositories.

### Tips
- third person
- when declarative statement, have to back up claim, can use online reference
- who else has worked on this?
- scripps will have good network references
- dont have to do research myself, note each claim and then can fill in
- dont use qualifiers, such as "furthermore"
- cut out fluff, make it succinct, to the point, convey a message / tell a story

## Github API
- [Github API](https://developer.github.com/v3/)
- [Github Oauth](https://developer.github.com/v3/oauth/)
- [Generate API Token](https://help.github.com/articles/creating-an-access-token-for-command-line-use/)

### Rate Limits
- Unauthenticated: 60 per hour
- Authenticated: 5000 per hour
- Search: 30 per hour

### Github Network
- [Most Followed Github Users](https://gist.github.com/paulmillr/2657075/)
- [Most Starred Repositories](https://github.com/search?o=desc&q=stars%3A%3E1&s=stars&type=Repositories)

### GoLang Github API
- [Go Github API by Google](https://github.com/google/go-github)

### Examples
- [My Followers](https://api.github.com/users/baldwmic/followers)
- [My Repos](https://api.github.com/users/baldwmic/repos)



## Vagrant
- [Project Setup](https://docs.vagrantup.com/v2/getting-started/project_setup.html)
- [Save Changes to Vagrant Box](http://stackoverflow.com/questions/22181325/saving-and-sharing-changes-made-to-vagrant-box)
- [Provisioning](http://docs.vagrantup.com/v2/getting-started/provisioning.html)

### Commands
#### start vagrant
```   
    vagrant up
```
#### login into virtual machine
```
    vagrant ssh
```

### Vagrant Box
- [Create Vagrant Box from Existing One](https://scotch.io/tutorials/how-to-create-a-vagrant-base-box-from-an-existing-one)



### Linux
- [User management in Linux](http://askubuntu.com/questions/410244/a-command-to-list-all-users-and-how-to-add-delete-modify-users)
- [Switch users in terminal](http://unix.stackexchange.com/questions/3568/how-to-switch-between-users-on-one-terminal)
- [Run command for another user](http://superuser.com/questions/468161/howto-switch-chage-user-id-witin-a-bash-script-to-execute-commands-in-the-same/468163#468163)

### Git
- [merge branch into master overwrite](http://stackoverflow.com/questions/2862590/how-to-replace-master-branch-in-git-entirely-from-another-branch)
