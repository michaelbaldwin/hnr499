# [hnr499](http://www.gvsu.edu/honor/the-senior-project-43.htm) (Github Social Network Analysis)

## Idea:
- Developers use Github. Github hosts data. Let's analyze that data to learn about how we write code.

### Contributors
- Student: Michael Baldwin
- Mentor: Dr. Roger Ferguson
- Data Mining Lead: Dr. Jerry Scripps

## Project Management
- [Project Status](https://trello.com/b/b3GYGihG/hnr-499)
- [Time Tracking](https://support.toggl.com/bookmark-reports/)

### Technology Stack
- Github API Client: [go-github](https://github.com/google/go-github)
- Data Preprocessing: [Python 2.7.6](https://www.python.org/)
- Graph Database: [Neo4j Community Edition 2.3.2](http://neo4j.com/)
- Neo4j Client: [py2neo](https://github.com/nigelsmall/py2neo)
- Data Visualization: [Plotly](https://plot.ly/python/)
- Library Helpers:
  - [GoLang Github API](https://bitbucket.org/michaelbaldwin/golang-github)
  - [GoLang Data Structures](https://bitbucket.org/michaelbaldwin/golang-datastructures)

## Development Environment
- [Setup instructions](https://bitbucket.org/michaelbaldwin/hnr499/src/master/docs/Project/contribute.md)

## Run Project
```
time sh src/bash/hnr499.sh &
```
