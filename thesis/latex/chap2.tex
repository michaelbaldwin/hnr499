%% This is an example first chapter.  You should put chapter/appendix that you
%% write into a separate file, and add a line \include{yourfilename} to
%% main.tex, where `yourfilename.tex' is the name of the chapter/appendix file.
%% You can process specific files by typing their names in at the
%% \files=
%% prompt when you run the file main.tex through LaTeX.
\chapter{Design and Methodology}

Computational analysis on open source software projects was used to demonstrate
that a few experts in software engineering limit the diversity of thought in the
open source community. Social network analysis was employed to examine the
social hierarchy of the open source community.

\section{Social Network Analysis}
Social network analysis maps and measures relationships among entities in a
network \cite{social-network-analysis}. Social networks are often represented
using a mathematical subfield known as graph theory; in graph theory, entities
are called vertices, while relationships between entities are known as edges
\cite{social-network-graph-theory}. By representing a social network as a graph,
mathematical properties about the graph can be calculated to better explain the
nature of the social network. A social network analysis of social relationships
present in the open source community requires data to construct the social
network of software engineers contributing to open source projects.

\section{Github}
While other websites also provide online hosting for software projects, the
web-based Git repository hosting service Github was chosen for a few key
reasons:
\begin{itemize}
    \item Github provides an Application Programming Interface (API), which
    provides convenient and efficient access to structured data about software
    projects hosted on Github.
    \item Not only does Github host active software projects, but it also acts
    as a social network, connecting users to projects they have contributed to,
    as well as other users on the Github platform.
    \item Github's popularity, with over 10 million repositories in 2013
    \cite{github-popularity}, proves its relevance in today's open source
    community.
\end{itemize}
A Github repository holds the entire history of source code for a software
project \cite{github-glossary-repository}, allowing one to reasonably consider
repositories as synonymous for projects. A Github repository can have multiple
collaborators who contributed to that software project. A Github user can follow
other users or be followed by other users; this relationship is bidirectional in
that following another user does not require a mutual follow from the other
user. The social network analysis tool implemented was generalized to construct
a social network of Github users from any number of Github repositories
selected.

\section{Social Network Analysis Tool}
Designed to further research on diversity of thought in software development,
the social network analysis tool was architected using several software modules
written in Go, Python and Bash. Together, these modules generated and analyzed
a social network by executing in an orchestrated, sequential manner. To
accomplish this, the Github API was queried for specific data on a given set
of Github repositories, the queried data was preprocessed for relationships
between users, a Neo4j graph database was created from the preprocessed data,
and the resulting database holding the social network was analyzed for
meaningful relationships.

\subsection{Github API}
To understand the social relationships present in a Github repository, the
Github API was used to query the contributors of that repository and which
users those contributors followed (Listing \ref{go-code}).

\begin{lstlisting}[caption={Query Github API in Go},label={go-code}]
// get list of contributors for each repository
repositories := m.GetRepositoryData(m.Config.Repositories)
serialize.WriteJson("data/github/repositories.json",
                    repositories)

// get those users each contributor follows
users := m.GetUserData(repositories)
serialize.WriteJson("data/github/users.json", users)
\end{lstlisting}

Following a user on Github is similar to following a user on other social
networks with activity feeds. By following another user on Github, the follower
subscribes to visible updates highlighting the followed user's actions on the
Github platform. While one might follow another user on Github for any number of
reasons, one such motivation could be to stay updated about the user's
contributions to software projects on Github.

\subsection{Neo4j Graph Database}
To construct a social network of software engineers who contribute to open
source projects, the queried data was converted to a graph representation and
imported into Neo4j, a database designed to store data in a native graph format.
The data queried from Github was preprocessed to identify unique vertices and
edges. Github repositories and users were converted to vertices, while
contributions and follows were preprocessed as edges (Listing \ref{python-code}).

\begin{lstlisting}[caption={Convert Data to Graph Format in Python},label={python-code},language={Python}]
# preprocess data for repositories
repositories_json = 'data/github/repositories.json'
repositories_csv = 'data/neo4j/nodes/repositories.csv'
repositories.create_csv(repositories_json,
                        repositories_csv)

# preprocess data for users
users_json = 'data/github/users.json'
users_csv = 'data/neo4j/nodes/users.csv'
users.create_csv(users_json, users_csv)

# preprocess data for follows
follows_csv = 'data/neo4j/relationships/follows.csv'
follows.create_csv(users_csv, users_json, follows_csv)

# preprocess data for contributes
contributes_csv = 'data/neo4j/relationships/contributes.csv'
contributes.create_csv(users_csv, repositories_csv,
                       repositories_json, contributes_csv)
\end{lstlisting}

After querying and preprocessing the Github data, the graph data was imported
into the Neo4j database to create the social network. A real Github user who
contributes to a popular open source project called Bootstrap was visualized.
(Figure \ref{fig:neo4j}). While many more users than shown follow this user's
Github activity, they have been eliminated from view to highlight the social
relationships present in the network.

\begin{figure}
\caption{Neo4j Graph Database Storing Social Network.}
\includegraphics[width=1.0\textwidth]{img/neo4j_viz.png}
\label{fig:neo4j}
\end{figure}

\subsection{Degree Distribution Histogram}

With the notion that software development experts on the Github platform would
be followed by many other Github users for their technical prowess and
involvement in the open source community, the constructed graph was queried for
its degree distribution. In graph theory, the degree of a vertex is the number
of edges which touch the vertex \cite{vertex-degree}. Since the generated
network is a directed graph, one in which edges are bidirectional, the
distribution for both in-degree, the number of inward edges to a vertex, and
out-degree, the number of outward edges from a vertex, was calculated
(Listing \ref{cypher-code}).

\begin{lstlisting}[caption={Query Degree Distribution in Cypher},label={cypher-code}]
MATCH (n:User)
WITH n,
     size((n)-[:FOLLOWS]->()) as out_degree,
     size((n)<-[:FOLLOWS]-()) as in_degree
RETURN n, out_degree, in_degree,
       out_degree+in_degree as degree
\end{lstlisting}

For the follows edge, a large in-degree indicates many other Github users follow
that user, while a large out-degree means that the Github user follows many
other users. By computing the degree distribution of the network and plotting
it as a histogram, a data visualization was created to better understand the
relationships between software developers contributing to Github repositories.
