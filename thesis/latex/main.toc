\contentsline {chapter}{\numberline {1}Introduction}{7}
\contentsline {section}{\numberline {1.1}Diversity of Thought}{7}
\contentsline {section}{\numberline {1.2}Open Source Software}{8}
\contentsline {section}{\numberline {1.3}Software Engineer Experts}{8}
\contentsline {chapter}{\numberline {2}Design and Methodology}{9}
\contentsline {section}{\numberline {2.1}Social Network Analysis}{9}
\contentsline {section}{\numberline {2.2}Github}{9}
\contentsline {section}{\numberline {2.3}Social Network Analysis Tool}{10}
\contentsline {subsection}{\numberline {2.3.1}Github API}{11}
\contentsline {subsection}{\numberline {2.3.2}Neo4j Graph Database}{11}
\contentsline {subsection}{\numberline {2.3.3}Degree Distribution Histogram}{12}
\contentsline {chapter}{\numberline {3}Results and Discussion}{15}
\contentsline {section}{\numberline {3.1}Github Repositories}{15}
\contentsline {subsection}{\numberline {3.1.1}MHacks-iOS}{15}
\contentsline {subsection}{\numberline {3.1.2}Bootstrap}{16}
\contentsline {section}{\numberline {3.2}Diversity of Thought in Software Engineering}{17}
\contentsline {chapter}{\numberline {4}Conclusion}{19}
\contentsline {section}{\numberline {4.1}Lessons Learned}{19}
\contentsline {section}{\numberline {4.2}Future Work}{20}
\contentsline {section}{\numberline {4.3}Acknowledgements}{20}
