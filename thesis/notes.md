# HNR 499 Paper
1. Write thesis statement / hypothesis
2. Develop outline for paper
3. Draft the content.
4. Review / edit.
5. Rinse and repeat.

## thesis
- http://cs.fit.edu/~wds/guides/howto/howto.html


## writing
- http://cc.oulu.fi/~smac/TRW/tense_abstract.htm
- http://writingcenter.unlv.edu/writing/abstract.html

## Latex Template

### Sections
- Cover -> cover.tex
- Abstract -> abstract.tex
- Table of Contents -> main.toc (generated automatically!)
- Paper Content -> main.tex

### Don't Need
- Proposal Cover -> propcover.tex
- Signature -> signature.tex

## Latex

### Editing
-

### Formatting
#### Code Highlighting
- http://stackoverflow.com/questions/3175105/writing-code-in-latex-document
#### Caption for Listings
- http://tex.stackexchange.com/questions/54819/listings-caption

### Compiling to PDF
```
Open TexShop, set TypeSet to "pdflatexmk" and click "TypeSet."
```
- http://tex.stackexchange.com/questions/204291/bibtex-latex-compiling
- http://tex.stackexchange.com/questions/53235/why-does-latex-bibtex-need-three-passes-to-clear-up-all-warnings
